class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
    @user = User.includes({:records => :rock}, {:records => :area}, {:records => :route}).find(params[:id])
    @records = @user.records.order("records.day DESC").page(params[:page])
    @grades = Record.joins(:route).where(user_id: params[:id]).group("records.result, routes.grade").order("routes.grade").pluck("routes.grade, records.result, count('id')")
    @lines = @grades.group_by{ |grade| grade[0] }
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Sent confirmation mail. Finish registration with click the link."
      redirect_to root_url
    else
      render 'new'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :user_name, :email, :password, :password_confirmation)
    end
end
