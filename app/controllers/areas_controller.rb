class AreasController < ApplicationController
  attr_accessor :region_id

  def index
    if current_user.admin?
      @areas = Area.all
    else
      redirect_to root_url
    end
  end

  def new
    @area = Area.new
  end

  def create
    @area = Area.new(area_params)
    if @area.save
      redirect_to Region.find(params[:region][:region_id])
    else
      redirect_to 'new'
    end
  end

  def import
    Area.import(params[:file])
    redirect_to areas_path, notice: "Added areas"
  end

  private

  def area_params
    params[:area].permit(:rock_id, :name, :region_id)
  end
end
