class RoutesController < ApplicationController
  attr_accessor :region_id

  def index
    if current_user.admin?
      @routes = Route
      .includes(:area)
      .all
      .order(:area_id, :id)
    else
      redirect_to root_url
    end
  end

  def new
    @route = Route.new
  end

  def create
    @route = Route.new(route_params)
    if @route.save
      redirect_to Region.find(params[:region][:region_id])
    else
      redirect_to 'new'
    end
  end

  def import
    Route.import(params[:file])
    redirect_to routes_path, notice: "Added routes"
  end

  private

    def route_params
      params[:route].permit(:area_id, :name, :grade, :region_id, :kind, :rate)
    end
end
