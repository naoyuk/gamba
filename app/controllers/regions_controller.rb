class RegionsController < ApplicationController
  def index
    #if current_user.admin?
      @regions = Region.search(params[:search]).where(able: true)
    #else
      #redirect_to root_url
    #end
  end

  def show
    @region = Region.includes(rocks: [areas: [:routes]]).find(params[:id])
  end

  def destroy
    @region = Region.find(params[:id])
    @region.destroy
  end

  def import
    Region.import(params[:file])
    redirect_to regions_path, notice: "Added regions"
  end
end
