class RocksController < ApplicationController
  def index
    if current_user.admin?
      @rocks = Rock.includes(:region).all
    else
      redirect_to root_url
    end
  end

  def new
    @rock = Rock.new
  end

  def create
    @rock = Rock.new(rock_params)
    region_id = params[:rock][:region_id]
    if @rock.save
      redirect_to Region.find(region_id)
    else
      redirect_to 'new'
    end
  end

  def import
    Rock.import(params[:file])
    redirect_to rocks_path, notice: "Added rocks"
  end

  private

  def rock_params
    params[:rock].permit(:region_id, :name)
  end
end
