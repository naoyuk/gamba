class RecordsController < ApplicationController
  before_action :set_record, only: [:show, :edit, :update]

  def index
    @records = Record
    .includes(:user, :area, :route, :rock)
    .search_some_cols(params[:search])
    .order("id DESC")
    .page(params[:page])
  end

  def show
  end

  def new
    @record = Record.new
    # rock_id = Area.find(params[:area_id]).rock_id
    # @record.rock_id = rock_id
    # @record.area_id = params[:area_id]
    # @record.route_id = params[:route_id]
    # @region_id = Rock.find(rock_id).region_id
  end

  def create
    @record = current_user.records.build(record_params)
    if @record.save
      redirect_to records_path
    else
      render 'new'
    end
  end

  def edit
    @regions_select = Region.select("id, name")
  end

  def update
    if @record.update(record_params)
      redirect_to record_path(params[:id])
    else
      render 'edit'
    end
  end

  def destroy
  end

  def rocks_select
    if request.xhr?
      render partial: 'rocks', locals: {kind: params[:kind], region_id: params[:region_id]}
    end
  end

  def areas_select
    if request.xhr?
      render partial: 'areas', locals: {rock_id: params[:rock_id]}
    end
  end

  def routes_select
    if request.xhr?
      render partial: 'routes', locals: {area_id: params[:area_id]}
    end
  end

  private

    def record_params
      params[:record].permit(:rock_id, :area_id, :route_id, :grade, :day, :result, :memo, :picture)
    end

    def set_record
      @record = Record.find(params[:id])
    end
end
