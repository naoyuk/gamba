# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# 種別で"ルート"を選択した時の岩場のセレクトの絞込み処理
$(document).on 'change', '#kind_0', ->
  if $(this).val() == ""
    $('#record_region_box').hide(500)
    $('#record_rock_box').hide(500)
    $('#record_area_box').hide(500)
    $('#record_route_box').hide(500)
  else
    if $('#region_id').val() != ""
      $.ajax(
        type: 'GET'
        url: '/records/rocks_select'
        data: {
          kind: 0
          region_id: $("#region_id").val()
        }
      ).done (data) ->
        $('#record_rock_id').html(data)
        $('#record_area_id').val("")
        $('#record_route_id').val("")
      $('#record_rock_box').show(500)
      $('#record_area_box').hide(500)
      $('#record_route_box').hide(500)
    else
      $('#record_region_box').show(500)
      $('#record_rock_box').hide(500)
      $('#record_area_box').hide(500)
      $('#record_route_box').hide(500)

# 種別で"ボルダー"を選択した時の岩場のセレクトの絞込み処理
$(document).on 'change', '#kind_1', ->
  if $(this).val() == ""
    $('#record_region_box').hide(500)
    $('#record_rock_box').hide(500)
    $('#record_area_box').hide(500)
    $('#record_route_box').hide(500)
  else
    if $('#region_id').val() != ""
      $.ajax(
        type: 'GET'
        url: '/records/rocks_select'
        data: {
          kind: 1
          region_id: $("#region_id").val()
        }
      ).done (data) ->
        $('#record_rock_id').html(data)
        $('#record_area_id').val("")
        $('#record_route_id').val("")
      $('#record_rock_box').show(500)
      $('#record_area_box').hide(500)
      $('#record_route_box').hide(500)
    else
      $('#record_region_box').show(500)
      $('#record_rock_box').hide(500)
      $('#record_area_box').hide(500)
      $('#record_route_box').hide(500)

# 種別で"クラック"を選択した時の岩場のセレクトの絞込み処理
$(document).on 'change', '#kind_2', ->
  if $(this).val() == ""
    $('#record_region_box').hide(500)
    $('#record_rock_box').hide(500)
    $('#record_area_box').hide(500)
    $('#record_route_box').hide(500)
  else
    if $('#region_id').val() != ""
      $.ajax(
        type: 'GET'
        url: '/records/rocks_select'
        data: {
          kind: 2
          region_id: $("#region_id").val()
        }
      ).done (data) ->
        $('#record_rock_id').html(data)
        $('#record_area_id').val("")
        $('#record_route_id').val("")
      $('#record_rock_box').show(500)
      $('#record_area_box').hide(500)
      $('#record_route_box').hide(500)
    else
      $('#record_region_box').show(500)
      $('#record_rock_box').hide(500)
      $('#record_area_box').hide(500)
      $('#record_route_box').hide(500)

# 種別で"トラッド"を選択した時の岩場のセレクトの絞込み処理
$(document).on 'change', '#kind_3', ->
  if $(this).val() == ""
    $('#record_region_box').hide(500)
    $('#record_rock_box').hide(500)
    $('#record_area_box').hide(500)
    $('#record_route_box').hide(500)
  else
    if $('#region_id').val() != ""
      $.ajax(
        type: 'GET'
        url: '/records/rocks_select'
        data: {
          kind: 3
          region_id: $("#region_id").val()
        }
      ).done (data) ->
        $('#record_rock_id').html(data)
        $('#record_area_id').val("")
        $('#record_route_id').val("")
      $('#record_rock_box').show(500)
      $('#record_area_box').hide(500)
      $('#record_route_box').hide(500)
    else
      $('#record_region_box').show(500)
      $('#record_rock_box').hide(500)
      $('#record_area_box').hide(500)
      $('#record_route_box').hide(500)

# 地方を選択した時の種別の表示/非表示
$(document).on 'change', '#region_id', ->
  if $(this).val() == ""
    $('#record_rock_box').hide(500)
    $('#record_area_box').hide(500)
    $('#record_route_box').hide(500)
    $("#kind_0").attr("checked", false)
    $("#kind_1").attr("checked", false)
  else
    $(document).on 'change', '#region_id', $ ->
      radi = $("input[name='kind']:checked").val()
      val_kind = if radi == '1' then 1 else 0

      $.ajax(
        type: 'GET'
        url: '/records/rocks_select'
        data: {
          kind: val_kind
          region_id: $("#region_id").val()
        }
      ).done (data) ->
        $('#record_rock_id').html(data)
        $('#record_area_id').val("")
        $('#record_route_id').val("")
    $('#record_rock_box').show(500)
    $('#record_area_box').hide(500)
    $('#record_route_box').hide(500)

# 岩場を選択した時のエリアのセレクトの絞込み処理
$(document).on 'change', '#record_rock_id', ->
  if $(this).val() == ""
    $('#record_area_box').hide(500)
    $('#record_route_box').hide(500)
  else
    $(document).on 'change', '#record_rock', $ ->
      $.ajax(
        type: 'GET'
        url: '/records/areas_select'
        data: {
          rock_id: $("#record_rock_id").val()
        }
      ).done (data) ->
        $('#areas_select').html(data)
        $('#record_area_id').val("")
        $('#record_route_id').val("")
    $('#record_area_box').show(500)
    $('#record_route_box').hide(500)

# エリアを選択した時のルートのセレクトの絞込み処理
$(document).on 'change', '#record_area_id', ->
  if $(this).val() == ""
    $('#record_route_box').hide(500)
  else
    $(document).on 'change', '#areas_select', $ ->
      $.ajax(
        type: 'GET'
        url: '/records/routes_select'
        data: {
          area_id: $("#record_area_id").val()
        }
      ).done (data) ->
        $('#routes_select').html(data)
        $('#record_route_id').val("")
    $('#record_route_box').show(500)

# 長い文字列を省略して「続きを読む」機能
ready = ->
  count = 20
  $('.text_overflow').each ->
    thisText = $(this).text()
    textLength = thisText.length
    if textLength > count
      showText = thisText.substring(0, count)
      hideText = thisText.substring(count, textLength)
      insertText = showText
      insertText += '<span class="hide">' + hideText + '</span>'
      insertText += '<span class="omit">…</span>'
      insertText += '<a href="" class="more">続きを読む</a>'
      $(this).html insertText
    return
  $('.text_overflow .hide').hide()
  $('.text_overflow .more').click ->
    $(this).hide().prev('.omit').hide().prev('.hide').fadeIn()
    false
  return

$(document).ready(ready)
$(document).on('page:load', ready)
