# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'change', '#rock_select', ->
  $.ajax(
    type: 'GET'
    url: '/routes/areas_select'
    data: {
      rock_id: $("#area_rock_id").val()
    }
  ).done (data) ->
    $('#areas_select').html(data)

$ ->
    $('.area').on 'click', ->
      num = $(this).attr('id').slice(5)
      $('.route_of_area_' + num).toggle()
      return
