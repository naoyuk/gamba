# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery(document).ready ->
    $('.side-bar .region_list .regions .region').on 'click', ->
        num = $(this).attr('id').slice(7)
        $('.region_list, .rock_list table tr').hide()
        $('#back_to_region, .rock_list').show()
        $('.region_id_' + num).show()
        return

$ ->
    $('#back_to_region').on 'click', ->
        $('.rock_list').hide()
        $('.region_list').show()
        return

$ ->
    $('.head_area').on 'click', ->
        num = $(this).attr('id').slice(10)
        $('.bottom_route_' + num).toggle()
        return
