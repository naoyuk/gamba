# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
    $('.area').on 'click', ->
        num = $(this).attr('id').slice(5)
        $('.area_list, .route_list table tr').hide()
        $('.route_list, #back_to_area').show()
        $('.area_id_' + num).show()
        return

$ ->
    $('#back_to_area').on 'click', ->
        $('.route_list').hide()
        $('.area_list').show()
        return
