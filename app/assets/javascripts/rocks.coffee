# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
    $('.rock').on 'click', ->
        num = $(this).attr('id').slice(5)
        $('.rock_list, .area_list table tr').hide()
        $('.area_list, #back_to_rock').show()
        $('.rock_id_' + num).show()
        return

$ ->
    $('#back_to_rock').on 'click', ->
        $('.area_list').hide()
        $('.rock_list').show()
        return
