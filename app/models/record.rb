class Record < ActiveRecord::Base
  belongs_to :user
  belongs_to :rock
  belongs_to :area
  belongs_to :route
  belongs_to :user

  mount_uploader :picture, PictureUploader

  validates :rock_id, presence: true
  validates :area_id, presence: true
  validates :route_id, presence: true
  validates :result, presence: true
  validates :day, presence: true
  validate  :picture_size

  scope :search_some_cols, lambda { |word|
    if word.blank?
      Record.all
    else
      search_word = word.downcase
      Record.joins(:rock, :area, :route, :user)
      .where(
        "lower(rocks.name) LIKE '%" + search_word + "%' or
        lower(areas.name) LIKE '%" + search_word + "%' or
        lower(routes.name) LIKE '%" + search_word + "%' or
        lower(routes.grade) LIKE '%" + search_word + "%' or
        lower(users.user_name) LIKE '%" + search_word + "%'"
        )
    end
  }

  private

    # アップロードされた画像のサイズをバリデーションする
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end

end
