class Rock < ActiveRecord::Base
  belongs_to :region
  has_many :zones
  has_many :areas
  has_many :records

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      rock = find_by(id: row["id"]) || new
      rock.attributes = row.to_hash.slice(*updatable_attributes)
      rock.save!
    end
  end

  def self.updatable_attributes
    ["name", "region_id"]
  end
end
