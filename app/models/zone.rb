class Zone < ActiveRecord::Base
  belongs_to :rock
  has_many :areas

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      zone = find_by(id: row["id"]) || new
      zone.attributes = row.to_hash.slice(*updatable_attributes)
      zone.save!
    end
  end

  def self.updatable_attributes
    ["name", "rock_id"]
  end
end
