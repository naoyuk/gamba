class Region < ActiveRecord::Base
  has_many :rocks

  def self.search(search)
    if search
      where(['name LIKE ?', "%#{search}%"])
    else
      all
    end
  end

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      region = find_by(id: row["id"]) || new
      region.attributes = row.to_hash.slice(*updatable_attributes)
      region.save!
    end
  end

  def self.updatable_attributes
    ["name", "able"]
  end
end
