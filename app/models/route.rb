class Route < ActiveRecord::Base
  belongs_to :area
  has_many :records

  validates :name, presence: true
  validates :grade, presence: true
  validates :kind, presence: true

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      route = find_by(id: row["id"]) || new
      route.attributes = row.to_hash.slice(*updatable_attributes)
      route.save!
    end
  end

  def self.updatable_attributes
    ["name", "area_id", "grade", "kind", "rate"]
  end

  def route_name_and_grade
    "#{self.name}(#{self.grade})"
  end

end
