class Area < ActiveRecord::Base
  belongs_to :rock
  belongs_to :zone
  has_many :routes
  has_many :records

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      area = find_by(id: row["id"]) || new
      area.attributes = row.to_hash.slice(*updatable_attributes)
      area.save!
    end
  end

  def self.updatable_attributes
    ["name", "rock_id"]
  end
end
