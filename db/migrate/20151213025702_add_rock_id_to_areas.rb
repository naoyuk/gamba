class AddRockIdToAreas < ActiveRecord::Migration
  def change
    add_column :areas, :rock_id, :integer
    add_index :areas, :rock_id
  end
end
