class AddColumnToRoutes < ActiveRecord::Migration
  def change
    add_column :routes, :grade, :string
    add_index :routes, :grade
  end
end
