class RemoveZoneIdFromAreas < ActiveRecord::Migration
  def change
    remove_column :areas, :zone_id, :integer
  end
end
