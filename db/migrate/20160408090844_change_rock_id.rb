class ChangeRockId < ActiveRecord::Migration
  def change
    rename_column :records, :rock_id, :rock_id_old
    rename_column :records, :area_id, :area_id_old
    rename_column :records, :route_id, :route_id_old
    add_column :records, :rock_id, :integer
    add_column :records, :area_id, :integer
    add_column :records, :route_id, :integer
    add_index :records, :rock_id
    add_index :records, :area_id
    add_index :records, :route_id
    
  end
end
