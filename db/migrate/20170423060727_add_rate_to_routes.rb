class AddRateToRoutes < ActiveRecord::Migration
  def change
    add_column :routes, :rate, :integer, default: 0
  end
end
