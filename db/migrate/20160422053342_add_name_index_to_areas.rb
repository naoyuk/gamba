class AddNameIndexToAreas < ActiveRecord::Migration
  def change
    add_index :areas, :name
  end
end
