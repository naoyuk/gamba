class AddNameIndexToRocks < ActiveRecord::Migration
  def change
    add_index :rocks, :name
  end
end
