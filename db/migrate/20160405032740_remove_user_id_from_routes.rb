class RemoveUserIdFromRoutes < ActiveRecord::Migration
  def change
    remove_column :routes, :user_id, :integer
  end
end
