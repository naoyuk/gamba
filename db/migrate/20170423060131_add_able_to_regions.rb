class AddAbleToRegions < ActiveRecord::Migration
  def change
    add_column :regions, :able, :boolean, default: false
  end
end
