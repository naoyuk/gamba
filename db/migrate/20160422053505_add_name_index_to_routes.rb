class AddNameIndexToRoutes < ActiveRecord::Migration
  def change
    add_index :routes, :name
  end
end
