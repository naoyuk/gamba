class AddKindToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :kind, :integer
  end
end
