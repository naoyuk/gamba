class RenameColumnToRecords < ActiveRecord::Migration
  def change
    rename_column :records, :rock, :rock_id
    rename_column :records, :area, :area_id
    rename_column :records, :route, :route_id
  end
end
