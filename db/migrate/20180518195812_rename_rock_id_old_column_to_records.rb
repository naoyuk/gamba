class RenameRockIdOldColumnToRecords < ActiveRecord::Migration
  def change
    rename_column :records, :rock_id_old, :picture
  end
end
