class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.string :rock
      t.string :area
      t.string :route
      t.date :day
      t.text :memo
      t.string :result

      t.timestamps null: false
    end
  end
end
