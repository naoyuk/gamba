class AddGradeToRecords < ActiveRecord::Migration
  def change
    add_column :records, :grade, :string
    add_index :records, :grade
  end
end
