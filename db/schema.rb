# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180518195812) do

  create_table "areas", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "rock_id"
  end

  add_index "areas", ["name"], name: "index_areas_on_name"
  add_index "areas", ["rock_id"], name: "index_areas_on_rock_id"

  create_table "records", force: :cascade do |t|
    t.string   "picture"
    t.string   "area_id_old"
    t.string   "route_id_old"
    t.date     "day"
    t.text     "memo"
    t.string   "result"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "grade"
    t.integer  "user_id"
    t.integer  "rock_id"
    t.integer  "area_id"
    t.integer  "route_id"
  end

  add_index "records", ["area_id"], name: "index_records_on_area_id"
  add_index "records", ["grade"], name: "index_records_on_grade"
  add_index "records", ["rock_id"], name: "index_records_on_rock_id"
  add_index "records", ["route_id"], name: "index_records_on_route_id"
  add_index "records", ["user_id"], name: "index_records_on_user_id"

  create_table "regions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "able",       default: false
  end

  create_table "rocks", force: :cascade do |t|
    t.string   "name"
    t.integer  "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "rocks", ["name"], name: "index_rocks_on_name"
  add_index "rocks", ["region_id"], name: "index_rocks_on_region_id"

  create_table "routes", force: :cascade do |t|
    t.string   "name"
    t.integer  "area_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "grade"
    t.integer  "kind"
    t.integer  "rate",       default: 0
  end

  add_index "routes", ["area_id"], name: "index_routes_on_area_id"
  add_index "routes", ["grade"], name: "index_routes_on_grade"
  add_index "routes", ["name"], name: "index_routes_on_name"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "user_name"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "users", ["email"], name: "index_users_on_email"
  add_index "users", ["user_name"], name: "index_users_on_user_name"

  create_table "zones", force: :cascade do |t|
    t.string   "name"
    t.integer  "rock_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "zones", ["rock_id"], name: "index_zones_on_rock_id"

end
