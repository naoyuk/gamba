require 'test_helper'

class RecordTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:naoyuki)
    @record = @user.records.build(route_id: 1)
  end
  
  test "should be valid" do
    assert @record.valid?
  end

  test "user id should be present" do
    @record.user_id = nil
    assert_not @record.valid?
  end
end
