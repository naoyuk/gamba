require 'rails_helper'

RSpec.describe Route, :type => :model do
  it "is valid with name, grade, kind, area_id and rate" do
    route = Route.new(
      name: "test_route_name",
      grade: "V4",
      kind: 1,
      area_id: 1,
      rate: 3,
    )
    expect(route).to be_valid
  end
end
