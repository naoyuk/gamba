require 'rails_helper'

RSpec.describe Record, :type => :model do
  # rock_id, area_id, route_id, result, dayがあれば有効となる
  it "is valid with rock_id, area_id, route_id, result, day" do
    record = Record.new(
      rock_id: 1,
      area_id: 1,
      route_id: 1,
      result: "Onsight",
      day: "2018-04-11",
    )
    expect(record).to be_valid
  end

  # resultがなければ無効となる
  it "is invalid without result" do
    record = Record.new(
      result: nil,
    )
    record.valid?
    expect(record).to_not be_valid
  end

  # dayがなければ無効となる
  it "is invalid without day" do
    record = Record.new(
      day: nil,
    )
    record.valid?
    expect(record).to_not be_valid
  end

  # 文字列を入力して記録の検索をする
  describe "search records with a term" do

    before do
      @user = User.new(
        id: 1,
        name: "Naoyuki",
        user_name: "naoyuki",
        email: "naoyuki@example.com",
        password: "password",
      )

      @rock = Rock.create(
        id: 1,
        region_id: 1,
        name: "備中",
      )

      @area = @rock.areas.create(
        id: 1,
        name: "長屋坂",
      )

      @route1 = @area.routes.create(
        id: 1,
        name: "フリッカー",
        grade: "5.11c",
        kind: "sport",
      )

      @route2 = @area.routes.create(
        id: 2,
        name: "パイナップルクライシス",
        grade: "5.11b",
        kind: "sport",
      )

      @route3 = @area.routes.create(
        id: 3,
        name: "黒島の交差点",
        grade: "5.11c",
        kind: "sport",
      )

      @record1 = @route1.records.create(
        rock_id: 1,
        area_id: 1,
        user_id: 1,
        result: "Onsight",
        day: "2018-04-11",
      )

      @record2 = @route2.records.create(
        rock_id: 1,
        area_id: 1,
        user_id: 1,
        result: "Flash",
        day: "2018-04-11",
      )

      @record3 = @route3.records.create(
        rock_id: 1,
        area_id: 1,
        user_id: 1,
        result: "Onsight",
        day: "2018-04-12",
      )

      @record4 = @route1.records.create(
        rock_id: 1,
        area_id: 1,
        user_id: 1,
        result: "Send",
        day: "2018-04-12",
      )
    end

    # 検索条件を入力していない場合は全件を拾う
    it "gets all records when you execute search without term"

    # ヒットする場合
    context "when matched datas are found"
      # 岩場名で検索条件に合う記録がある場合は、それらの記録を拾う
      it "gets matched records when you execute search with rock name"

      # エリア名で検索条件に合う記録がある場合は、それらの記録を拾う
      it "gets matched records when you execute search with area name"

      # ルート名で検索条件に合う記録がある場合は、それらの記録を拾う
      it "gets matched records when you execute search with route name"

      # グレードで検索条件に合う記録がある場合は、それらの記録を拾う
      it "gets matched records when you execute search with grade"

      # ユーザー名で検索条件に合う記録がある場合は、それらの記録を拾う
      it "gets matched records when you execute search with user name"

    # ヒットしない場合
    it "when matched datas are not found" do
      expect(Record.search_some_cols("忍者返し")).to be_empty
    end
  end




end
