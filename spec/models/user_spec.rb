require 'rails_helper'

RSpec.describe User, :type => :model do

  # 有効なファクトリを持つこと
  it "has a valid factory" do
    expect(FactoryGirl.build(:user)).to be_valid
  end

  # name, user_name, email, passwordが全てあれば有効になる
  it "is valid with name, user_name, email and password" do
    expect(FactoryGirl.build(:user)).to be_valid
  end

  # nameがなければ無効となる
  it "is invalid without name" do
    user = FactoryGirl.build(:user, name: nil)
    user.valid?
    expect(user.errors[:name]).to include("can't be blank")
  end

  # user_nameがなければ無効となる
  it "is invalid without user_name" do
    user = FactoryGirl.build(:user, user_name: nil)
    user.valid?
    expect(user.errors[:user_name]).to include("can't be blank")
  end

  # emailがなければ無効となる
  it "is invalid without email" do
    user = FactoryGirl.build(:user, email: nil)
    user.valid?
    expect(user.errors[:email]).to include("can't be blank")
  end

  # passwordがなければ無効となる
  it "is invalid without password" do
    user = FactoryGirl.build(:user, password: nil)
    user.valid?
    expect(user.errors[:password]).to include("can't be blank")
  end

  # passwordが5文字だと無効となる
  it "is invalid with a 5 letter password" do
    user = FactoryGirl.build(:user, password: "abcde")
    user.valid?
    expect(user.errors[:password]).to include("is too short (minimum is 6 characters)")
  end

  # passwordが6文字だと有効となる
  it "is valid with a 6 letter password" do
    expect(FactoryGirl.build(:user, password: "abcdef")).to be_valid
  end

  # メールアドレスが重複している場合は無効となる
  it "is invalid with a duplicate email address" do
    FactoryGirl.create(:user, email: "test@example.com")
    user = FactoryGirl.build(:user, email: "test@example.com")
    user.valid?
    expect(user.errors[:email]).to include("has already been taken")
  end

end
