# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    name "Naoyuki"
    sequence(:email) { |n| "naoyuki#{n}@example.com" }
    user_name "naoyuki"
    password "password"
  end
end
