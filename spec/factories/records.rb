# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :record do
    grade "5.12a"
    day "2018-04-12"
    result "Send"
    memo "this memo is test."
    association :rock
    association :area
    association :route
    association :user
  end
end
