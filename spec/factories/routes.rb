# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :route do
    name "Test Direct"
    grade "V6"
    kind 1
    association :region
    association :area
  end
end
